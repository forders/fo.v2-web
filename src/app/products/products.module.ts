import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductSetupComponent } from './product-setup/product-setup.component';
import { BrandComponent } from './product-setup/brand/brand.component';
import { UomComponent } from './product-setup/uom/uom.component';
import { CategoryComponent } from './product-setup/category/category.component';
import { AccordionModule } from '../ui-elements/basic/accordion/accordion.module';
import { UiSwitchModule } from 'ng2-ui-switch';
import { DataTableModule } from 'angular2-datatable';
import { AddEditUomComponent } from './product-setup/uom/add-edit-uom/add-edit-uom.component';
import { AddEditBrandComponent } from './product-setup/brand/add-edit-brand/add-edit-brand.component';
import { AddEditCategoryComponent } from './product-setup/category/add-edit-category/add-edit-category.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProductsRoutingModule,
    AccordionModule,
    UiSwitchModule,
    DataTableModule
  ],
  declarations: [
    ProductSetupComponent,
    BrandComponent,
    UomComponent,
    CategoryComponent,
    AddEditUomComponent,
    AddEditBrandComponent,
    AddEditCategoryComponent
  ]
})
export class ProductsModule { }
