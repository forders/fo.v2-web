import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductSetupComponent } from './product-setup/product-setup.component';

const routes: Routes = [

  {
    path: '',
    data: {
      title: 'Products',
      status: false
    },

    children: [
      {
        path: 'setup',
        component: ProductSetupComponent
      }
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
