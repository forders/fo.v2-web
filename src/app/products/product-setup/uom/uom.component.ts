import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uom',
  templateUrl: './uom.component.html',
  styleUrls: ['./uom.component.scss']
})
export class UomComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  constructor() {
    this.data = [
      {
        uomname: 'Kilogram', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin', status: 'Active'
      },
      {
        uomname: 'Piece', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin', status: 'Active'
      }
    ];
  }

  ngOnInit() {
  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }

}
