import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditUomComponent } from './add-edit-uom.component';

describe('AddEditUomComponent', () => {
  let component: AddEditUomComponent;
  let fixture: ComponentFixture<AddEditUomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditUomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditUomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
