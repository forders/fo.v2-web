import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  constructor() {
    this.data = [
      {
        brandname: 'Nike', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin', status: 'Active'
      },
      {
        brandname: 'Piece', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin', status: 'Active'
      }
    ];
  }

  ngOnInit() {
  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }
}
