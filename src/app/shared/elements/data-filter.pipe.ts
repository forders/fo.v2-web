import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "dataFilter"
})

export class DataFilterPipe implements PipeTransform {

    // transform(array: any[], query: string): any {
    //     console.log(query);
    //     console.log(array);
    //     if (query) {
    //         _.filter(array,function(roe){
    //             console.log(roe);
    //         })
    //         return _.filter(array, row=>row.name.indexOf(query) > -1);
    //     }
    //     return array;
    // }

    // transform(items: any[], searchText: string): any[] {
    //     if (!items) return [];
    //     if (!searchText) return items;
    //     searchText = searchText.toLowerCase();
    //     return items.filter(it => {
    //         console.log(it)
    //         return it.toLowerCase().includes(searchText);
    //     });
    // }


    transform(items: any[], searchText: any): any {
        if (!items) return [];
        if (!searchText) return items;
        return items.filter(item =>{
           for (let key in item ) {
             if((""+item[key]).toLocaleLowerCase().includes(searchText.toLocaleLowerCase())){
                return (""+item[key]).toLocaleLowerCase().includes(searchText.toLocaleLowerCase());
             }
           }
           //return false;
        });
    }
}