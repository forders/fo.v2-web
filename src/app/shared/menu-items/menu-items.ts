import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: '',
    main: [
      {
        state: 'dashboard',
        short_label: 'D',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-home',
      },

      {
        state: 'masters',
        short_label: 'M',
        name: 'Masters',
        type: 'sub',
        icon: 'ti-user',
        children: [
          {
            state: 'users',
            name: 'Users'
          },
          {
            state: 'roles',
            name: 'Roles'
          },
          {
            state: 'employees',
            name: 'Employees'
          },
          {
            state: 'tenants',
            name: 'Tenants'
          },
          {
            state: 'industry',
            name: 'Industry'
          },
          {
            state: 'designation',
            name: 'Designation'
          },
          {
            state: 'sequence',
            name: 'Sequence '
          },

          {
            state: 'location',
            name: 'Location'
          },
          {
            state: 'currency',
            name: 'Currency'
          }
        ]
      },
      {
        state: 'products',
        short_label: 'P',
        name: 'Products',
        type: 'sub',
        icon: 'ti-shopping-cart',
        children: [
          {
            state: 'setup',
            name: 'Setup',
          },
          {
            state: 'product',
            name: 'Product',
          },
        ]
      },
    ],
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}
