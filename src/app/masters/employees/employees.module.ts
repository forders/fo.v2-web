import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './employees.component';
import { EmployeesRoutingModule } from './employees-routing.module';
import { HttpModule } from '@angular/http';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AddEditEmployeesComponent } from './add-edit-employees/add-edit-employees.component';
import { UiSwitchModule } from 'ng2-ui-switch';

@NgModule({
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    HttpModule,
    DataTableModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    UiSwitchModule
  ],
  declarations: [EmployeesComponent, AddEditEmployeesComponent],
  bootstrap:[EmployeesComponent]
})
export class EmployeesModule { }
