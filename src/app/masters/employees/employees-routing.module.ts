import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from '../employees/employees.component';
import { AddEditEmployeesComponent } from './add-edit-employees/add-edit-employees.component';
const routes: Routes = [
  {
    path: '',
    component: EmployeesComponent,
    data: {
      title: 'Employees',
      icon: 'ti-settings',
      caption: 'employees',
      status: false
    }
  },
  {
    path: 'create',
    component: AddEditEmployeesComponent,
    data: {
      title: 'Add Employees',
      icon: 'ti-settings',
      caption: 'add employees',
      status: false
    }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
