import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-add-edit-employees',
  templateUrl: './add-edit-employees.component.html',
  styleUrls: ['./add-edit-employees.component.scss']
})
export class AddEditEmployeesComponent implements OnInit {

  @Input('modalDefault') modalDefault: any;

  ngOnInit() {
  }

}
