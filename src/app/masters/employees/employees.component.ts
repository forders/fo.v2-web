import { ChangeDetectionStrategy, Component, OnInit, Input } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  uploader: FileUploader = new FileUploader({
    url: URL,
    isHTML5: true
  });

  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;

  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public userProPic: string;

  @Input('modalDefault') modalDefault: any;

  constructor() { }

  ngOnInit() {
    this.data = [
      {
        employeename: 'Pavithra', employeecode: '001', emailid: '	pavithra101@gmail.com', mobileno: '7425631524', designation: 'Developer', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        employeename: 'Priyanka', employeecode: '002', emailid: '	priyankapc@gmail.com', mobileno: '8765431524', designation: 'Admin', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        employeename: 'pavi', employeecode: '003', emailid: '	pavi@gmail.com', mobileno: '8765431524', designation: 'Manager', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      }
    ];
  }

}

