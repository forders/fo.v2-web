import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndustryComponent } from './industry/industry.component';
import { DesignationComponent } from './designation/designation.component';
import { SequenceSettingsComponent } from './sequence-settings/sequence-settings.component';
import { LocationComponent } from './location/location.component';
import { CurrencyComponent } from './currency/currency.component';


const routes: Routes = [

  {
    path: '',
    data: {
      title: 'Masters',
      status: false
    },

    children: [
      {
        path: 'users',
        loadChildren: './users/users.module#UsersModule'
      },
      {
        path: 'roles',
        loadChildren: './roles/roles.module#RolesModule'
      },
      {
        path: 'employees',
        loadChildren: './employees/employees.module#EmployeesModule'
      },
      {
        path: 'tenants',
        loadChildren: './tenants/tenants.module#TenantsModule'
      },
      {
        path: 'industry',
        component: IndustryComponent
      },
      {
        path: 'designation',
        component: DesignationComponent
      },
      {
        path: 'sequence',
        component: SequenceSettingsComponent
      },
      {
        path: 'location',
        component: LocationComponent
      },
      {
        path: 'currency',
        component: CurrencyComponent
      }

    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MastersRoutingModule { }
