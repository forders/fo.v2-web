import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-edit-industry',
  templateUrl: './add-edit-industry.component.html',
  styleUrls: ['./add-edit-industry.component.scss']
})
export class AddEditIndustryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }
}
