import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-industry',
  templateUrl: './industry.component.html',
  styleUrls: ['./industry.component.scss']
})
export class IndustryComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public userProPic: string;
  constructor() {
    this.data = [
      { industryname: 'FMCG Distribution', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
      { industryname: 'ABG Infralogistics Ltd', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
      { industryname: 'ARB Holdings Ltd', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' }
    ]
  }

  ngOnInit() {

  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }
}
