import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sequence-settings',
  templateUrl: './sequence-settings.component.html',
  styleUrls: ['./sequence-settings.component.scss']
})
export class SequenceSettingsComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  constructor() {
    this.data = [
      {
        description: 'Description to be entered here', prefixkey: 'ADJ', prefixconcatenate: '/', suffixkey: 'No', suffixconcatenate: '/', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        description: 'Description to be entered here', prefixkey: 'ADJ', prefixconcatenate: '/', suffixkey: 'No', suffixconcatenate: '/', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        description: 'Description entered here', prefixkey: 'ADJ', prefixconcatenate: '/', suffixkey: 'No', suffixconcatenate: '/', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      }
    ]
  }
  ngOnInit() {

  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }

}
