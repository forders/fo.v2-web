import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-sequence-settings',
  templateUrl: './edit-sequence-settings.component.html',
  styleUrls: ['./edit-sequence-settings.component.scss']
})
export class EditSequenceSettingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }
}
