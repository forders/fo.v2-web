import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSequenceSettingsComponent } from './edit-sequence-settings.component';

describe('EditSequenceSettingsComponent', () => {
  let component: EditSequenceSettingsComponent;
  let fixture: ComponentFixture<EditSequenceSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSequenceSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSequenceSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
