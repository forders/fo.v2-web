import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MastersRoutingModule } from './masters-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UiSwitchModule } from 'ng2-ui-switch';
import { IndustryComponent } from './industry/industry.component';
import { AddEditIndustryComponent } from './industry/add-edit-industry/add-edit-industry.component';
import { DesignationComponent } from './designation/designation.component';
import { AddEditDesignationComponent } from './designation/add-edit-designation/add-edit-designation.component';
import { SequenceSettingsComponent } from './sequence-settings/sequence-settings.component';
import { EditSequenceSettingsComponent } from './sequence-settings/edit-sequence-settings/edit-sequence-settings.component';
import { LocationComponent } from './location/location.component';
import { AccordionModule } from '../ui-elements/basic/accordion/accordion.module';
import { CurrencyComponent } from './currency/currency.component';
import { AddEditCurrencyComponent } from './currency/add-edit-currency/add-edit-currency.component';

@NgModule({
  imports: [
    CommonModule,
    MastersRoutingModule,
    SharedModule,
    HttpModule,
    DataTableModule,
    FormsModule,
    ReactiveFormsModule,
    UiSwitchModule,
    AccordionModule
  ],
  declarations: [IndustryComponent,
    AddEditIndustryComponent,
    DesignationComponent,
    AddEditDesignationComponent,
    SequenceSettingsComponent,
    EditSequenceSettingsComponent,
    LocationComponent,
    CurrencyComponent,
    AddEditCurrencyComponent],
})
export class MastersModule { }
