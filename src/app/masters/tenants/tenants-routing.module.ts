import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { TenantsComponent} from './tenants.component'
import { AddtenantsComponent} from './addtenants/addtenants.component'

const routes: Routes = [
  {
    path: '',
    component: TenantsComponent,
    data: {
      title: 'Tenants',
      icon: 'ti-settings',
      caption: 'tenants',
      status: false
    }
  },
  {
    path: 'create',
    component: AddtenantsComponent,
    data: {
      title: 'Add Tenants',
      icon: 'ti-settings',
      caption: 'Add tenants',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TenantsRoutingModule { }
