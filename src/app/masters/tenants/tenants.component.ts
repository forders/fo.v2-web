import { Component, OnInit, Input } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.scss']
})
export class TenantsComponent implements OnInit {

  uploader: FileUploader = new FileUploader({
    url: URL,
    isHTML5: true
  });

  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;

  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public userProPic: string;

  @Input('modalDefault') modalDefault: any;

  constructor() {
    this.data = [
      { tenantname: 'Mithun', emailid: 'mithun@gmail.com', mobileno: '7425631524', taxno: '123456123456', lastupdatedby: 'Admin', lastupdateddt: '02-12-1996' },
      { tenantname: 'Raj', emailid: 'mithun@gmail.com', mobileno: '7425631524', taxno: '123456123456', lastupdatedby: 'Admin', lastupdateddt: '02-12-1996' },
      { tenantname: 'Mithunraj', emailid: 'mithun@gmail.com', mobileno: '7425631524', taxno: '123456123456', lastupdatedby: 'Admin', lastupdateddt: '02-12-1996' },
    ]
  }

  ngOnInit() {
  }

}

