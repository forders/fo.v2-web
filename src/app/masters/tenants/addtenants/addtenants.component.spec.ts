import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtenantsComponent } from './addtenants.component';

describe('AddtenantsComponent', () => {
  let component: AddtenantsComponent;
  let fixture: ComponentFixture<AddtenantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtenantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtenantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
