import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TenantsRoutingModule } from './tenants-routing.module';
import { TenantsComponent} from './tenants.component'
import { HttpModule } from '@angular/http';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AddtenantsComponent } from './addtenants/addtenants.component';
import { UiSwitchModule } from 'ng2-ui-switch';

@NgModule({
  imports: [
    CommonModule,
    TenantsRoutingModule,
    HttpModule,
    DataTableModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    UiSwitchModule
  ],
  declarations: [TenantsComponent, AddtenantsComponent],
  bootstrap:[TenantsComponent]
})
export class TenantsModule { }
