import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-edit-currency',
  templateUrl: './add-edit-currency.component.html',
  styleUrls: ['./add-edit-currency.component.scss']
})
export class AddEditCurrencyComponent implements OnInit {
  public data: any;

  constructor() { }

  ngOnInit() {

  }

  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }
}
