import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fo-app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public userProPic: string;
  constructor() {
    this.data = [
      { currencyname: 'Rupees', currencysymbol: '₹	', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
      { currencyname: '	Euro', currencysymbol: '€', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
      { currencyname: 'Dollar', currencysymbol: '$', lastupdatedby: 'Pavithra', lastupdateddt: '03-12-2018' }

    ]
  }

  ngOnInit() {

  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }
}
