import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.scss']
})
export class DesignationComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public userProPic: string;

  constructor() {
    this.data = [
      { designationname: 'Developer', updatedby: 'Mithun', updateddt: '02-12-1996' },
      { designationname: 'Admin', updatedby: 'Mithun', updateddt: '02-12-1996' },
      { designationname: 'Manager', updatedby: 'Mithun', updateddt: '02-12-1996' }
    ]
  }

  ngOnInit() {

  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }
}
