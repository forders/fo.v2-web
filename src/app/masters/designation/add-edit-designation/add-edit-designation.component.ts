import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-edit-designation',
  templateUrl: './add-edit-designation.component.html',
  styleUrls: ['./add-edit-designation.component.scss']
})
export class AddEditDesignationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }
}
