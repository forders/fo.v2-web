import { Component, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';
import { IOption } from 'ng-select';
import { SelectOptionService } from '../../../shared/elements/select-option.service';

@Component({
  selector: 'app-addroles',
  templateUrl: './addroles.component.html',
  styleUrls: ['./addroles.component.scss']
})
export class AddrolesComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  public rolename: string;
  public userID: string;
  public userProPic: string;
  public userEmail: string;
  public userPosition: string;
  public userOffice: string;
  public userAge: number;
  public userContact: string;
  public userDate: string;

  simpleOption: Array<IOption> = this.selectOptionService.getCharacters();
  selectedOption = '4';
  isDisabled = true;
  characters: Array<IOption>;
  selectedCharacter = '4';
  timeLeft = 5;

  @Input('modalDefault') modalDefault: any;

  constructor(public selectOptionService: SelectOptionService) {
    this.data = [
      {
        rolename: 'Admin', dataccess: 'No', updatedby: 'View,Edit,Create,Delete', status: '-'
      },
      {
        rolename: 'Developer', dataccess: 'No', updatedby: 'View,Edit,Create,Delete', status: '-'
      },
      {
        rolename: 'Developer', dataccess: 'No', updatedby: 'View,Edit,Create,Delete', status: '-'
      },
      {
        rolename: 'Developer', dataccess: 'No', updatedby: 'View,Edit,Create,Delete', status: '-'
      },
      {
        rolename: 'Developer', dataccess: 'No', updatedby: 'View,Edit,Create,Delete', status: '-'
      },
      {
        rolename: 'Developer', dataccess: 'No', updatedby: 'View,Edit,Create,Delete', status: '-'
      },
    ];
  }

  ngOnInit() {

  }
}

