import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';

  @Input('modalDefault') modalDefault: any;

  constructor() {
    this.data = [
      {
        rolename: 'Admin', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        rolename: 'Developer', dataccess: 'Team', lastupdateddt: '26-Dec-2018 15:00', lastupdatedby: 'Admin'
      }
    ];
  }

  ngOnInit() {
  }

}
