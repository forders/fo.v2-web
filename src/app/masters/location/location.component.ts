import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-fo-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public userProPic: string;
  public firstArray: any;
  public secondArray: any;
  showDetails = false;
  showRegion = false;
  constructor() {
    this.data = [
      { statename: 'TamilNadu', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
      { statename: 'Kerala', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
      { statename: 'New Delhi', lastupdatedby: 'Pavithra', lastupdateddt: '03-12-2018' }

    ],
      this.firstArray = [
        { cityname: 'Salem', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
        { cityname: 'Coimbatore', lastupdatedby: 'Pavithra', lastupdateddt: '03-12-2018' },
        { cityname: 'Trichy', lastupdatedby: 'Pavithra', lastupdateddt: '04-12-2018' }

      ],
      this.secondArray = [
        { regionname: 'South', lastupdatedby: 'Pavithra', lastupdateddt: '02-12-2018' },
        { regionname: 'North', lastupdatedby: 'Pavithra', lastupdateddt: '03-12-2018' },
        { regionname: 'West', lastupdatedby: 'Pavithra', lastupdateddt: '04-12-2018' }

      ]
  }
  ngOnInit() {

  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }
  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }
}
