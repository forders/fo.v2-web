import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-add-edit-users',
  templateUrl: './add-edit-users.component.html',
  styleUrls: ['./add-edit-users.component.scss']
})
export class AddEditUsersComponent implements OnInit {

  @Input('modalDefault') modalDefault: any;

  ngOnInit() {
  }

}
