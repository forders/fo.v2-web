import { ChangeDetectionStrategy, Component, OnInit, Input } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  uploader: FileUploader = new FileUploader({
    url: URL,
    isHTML5: true
  });

  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;

  public data: any;
  public rowsOnPage = 8;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  public userProPic: string;

  @Input('modalDefault') modalDefault: any;

  constructor() {
    this.data = [
      {
        username: 'Pavithra', emailid: '	pavithra101@gmail.com', mobileno: '7425631524', lastlogin: '27-Nov-2018 20:32	', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        username: 'Priyanka', emailid: '	priyankapc@gmail.com', mobileno: '8765431524', lastlogin: '27-Nov-2018 20:32', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      },
      {
        username: 'pavi', emailid: '	pavi@gmail.com', mobileno: '8765431524', lastlogin: '27-Nov-2018 20:32', lastupdateddt: '25-Dec-2018 15:00', lastupdatedby: 'Admin'
      }
    ];
  }

  ngOnInit() {

  }
  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

}
